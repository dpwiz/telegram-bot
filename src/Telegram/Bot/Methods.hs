{-# LANGUAGE DataKinds, TypeFamilies #-}
module Telegram.Bot.Methods where

import Data.Aeson
import Data.Text.Lazy (Text)

import Telegram.Bot.API
import Telegram.Bot.Types

data GetMe = GetMe
    deriving (Eq, Show)

instance ToJSON GetMe where
    toJSON GetMe = object []

instance BotRequest GetMe where
    type BotMethod GetMe = "getMe"
    type BotResponse GetMe = User

data GetUpdates = GetUpdates
    { guOffset :: Maybe Integer
    , guLimit :: Maybe Integer
    , guTimeout :: Maybe Integer
    } deriving (Eq, Show)

instance ToJSON GetUpdates where
    toJSON (GetUpdates{..}) = object
        [ "offset" .= guOffset
        , "limit" .= guLimit
        , "timeout" .= guTimeout
        ]

instance BotRequest GetUpdates where
    type BotMethod GetUpdates = "getUpdates"
    type BotResponse GetUpdates = [Update]

data SendMessage = SendMessage
    { smChatId :: Either Integer Text
    , smText :: Text
    , smParseMode :: Maybe Text
    , smDisableWebPagePreview :: Maybe Bool
    , smReplyToMessageId :: Maybe Integer
    , smReplymarkup :: Maybe ()
    } deriving (Eq, Show)

instance ToJSON SendMessage where
    toJSON (SendMessage{..}) = object
        [ "chat_id" .= smChatId
        , "text" .= smText
        , "parse_mode" .= smParseMode
        , "disable_web_page_preview" .= smDisableWebPagePreview
        , "reply_to_message_id" .= smReplyToMessageId
        , "reply_markup" .= smReplymarkup
        ]

instance BotRequest SendMessage where
    type BotMethod SendMessage = "sendMessage"
    type BotResponse SendMessage = Message
