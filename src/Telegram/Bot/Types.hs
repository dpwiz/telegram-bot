module Telegram.Bot.Types where

import           Data.Aeson
import           Data.Monoid ((<>))
import           Data.Text.Lazy (Text)

data Update = Update
    { updateId                 :: Integer
    , updateMessage            :: Maybe Message
    , updateInlineQuery        :: Maybe InlineQuery
    , updateChosenInlineResult :: Maybe ChosenInlineResult
    } deriving (Eq, Show)

instance FromJSON Update where
    parseJSON = withObject "Update" $ \o -> Update
        <$> o .:  "update_id"
        <*> o .:? "message"
        <*> o .:? "inline_query"
        <*> o .:? "chosen_inline_result"

data User = User
    { userId        :: Integer
    , userFirstName :: Text
    , userLastName  :: Maybe Text
    , userUsername  :: Maybe Text
    } deriving (Eq, Show)

instance FromJSON User where
    parseJSON = withObject "User" $ \o -> User
        <$> o .:  "id"
        <*> o .:  "first_name"
        <*> o .:? "last_name"
        <*> o .:? "username"

data Chat = Chat
    { chatId        :: Integer
    , chatType      :: ChatType
    , chatTitle     :: Maybe Text
    , chatUsername  :: Maybe Text
    , chatFirstName :: Maybe Text
    , chatLastName  :: Maybe Text
    } deriving (Eq, Show)

instance FromJSON Chat where
    parseJSON = withObject "Chat" $ \o -> Chat
        <$> o .:  "id"
        <*> o .:  "type"
        <*> o .:? "title"
        <*> o .:? "username"
        <*> o .:? "first_name"
        <*> o .:? "last_name"

data ChatType
    = Private
    | Group
    | Supergroup
    | Channel
    deriving (Eq, Show)

instance FromJSON ChatType where
    parseJSON = withText "ChatType" $ \case
        "private"    -> pure Private
        "group"      -> pure Group
        "supergroup" -> pure Supergroup
        "channel"    -> pure Channel
        t            -> fail $ "Bad chat type: " <> show t

data Message = Message
    { messageId                    :: Integer
    , messageFrom                  :: Maybe User
    , messageDate                  :: Integer
    , messageChat                  :: Chat
    , messageForwardFrom           :: Maybe User
    , messageForwardDate           :: Maybe Integer
    , messageReplyTo               :: Maybe Message
    , messageText                  :: Maybe Text
    , messageAudio                 :: Maybe Audio
    , messageDocument              :: Maybe Document
    , messagePhoto                 :: [PhotoSize]
    , messageSticker               :: Maybe Sticker
    , messageVideo                 :: Maybe Video
    , messageVoice                 :: Maybe Voice
    , messageCaption               :: Maybe Text
    , messageContact               :: Maybe Contact
    , messageLocation              :: Maybe Location
    , messageNewChatParticipant    :: Maybe User
    , messageLeftChatParticipant   :: Maybe User
    , messageNewChatTitle          :: Maybe Text
    , messageDeleteChatPhoto       :: Bool
    , messageGroupChatCreated      :: Bool
    , messageSupergroupChatCreated :: Bool
    , messageChannelChatCreated    :: Bool
    , messageMigrateToChatId       :: Maybe Integer
    , messageMigrateFromChatId     :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Message where
    parseJSON = withObject "Message" $ \o -> Message
        <$> o .:  "message_id"
        <*> o .:? "from"
        <*> o .:  "date"
        <*> o .:  "chat"
        <*> o .:? "forward_from"
        <*> o .:? "forward_date"
        <*> o .:? "reply_to_message"
        <*> o .:? "text"
        <*> o .:? "audio"
        <*> o .:? "document"
        <*> o .:? "photo" .!= []
        <*> o .:? "sticker"
        <*> o .:? "video"
        <*> o .:? "voice"
        <*> o .:? "caption"
        <*> o .:? "contact"
        <*> o .:? "location"
        <*> o .:? "new_chat_participant"
        <*> o .:? "left_chat_participant"
        <*> o .:? "new_chat_title"
        <*> o .:? "delete_chat_photo" .!= False
        <*> o .:? "group_chat_created" .!= False
        <*> o .:? "supergroup_chat_created" .!= False
        <*> o .:? "channel_chat_created" .!= False
        <*> o .:? "migrate_to_chat_id"
        <*> o .:? "migrate_from_chat_id"

data PhotoSize = PhotoSize
    { photoFileId   :: Text
    , photoWidth    :: Integer
    , photoHeight   :: Integer
    , photoFileSize :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON PhotoSize where
    parseJSON = withObject "PhotoSize" $ \o -> PhotoSize
        <$> o .:  "file_id"
        <*> o .:  "width"
        <*> o .:  "height"
        <*> o .:? "file_size"

data Audio = Audio
    { audioFileId    :: Text
    , audioDuration  :: Integer
    , audioPerformer :: Maybe Text
    , audioTitle     :: Maybe Text
    , audioMimeType  :: Maybe Text
    , audioFileSize  :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Audio where
    parseJSON = withObject "Audio" $ \o -> Audio
        <$> o .:  "file_id"
        <*> o .:  "duration"
        <*> o .:? "performer"
        <*> o .:? "title"
        <*> o .:? "mime_type"
        <*> o .:? "file_size"

data Document = Document
    { documentFileId   :: Text
    , documentThumb    :: Maybe PhotoSize
    , documentFileName :: Maybe Text
    , documentMimeType :: Maybe Text
    , documentFileSize :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Document where
    parseJSON = withObject "Document" $ \o -> Document
        <$> o .:  "file_id"
        <*> o .:? "thumb"
        <*> o .:? "file_name"
        <*> o .:? "mime_type"
        <*> o .:? "file_size"

data Sticker = Sticker
    { stickerFileId   :: Text
    , stickerWidth    :: Integer
    , stickerHeight   :: Integer
    , stickerThumb    :: Maybe PhotoSize
    , stickerFileSize :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Sticker where
    parseJSON = withObject "Sticker" $ \o -> Sticker
        <$> o .:  "file_id"
        <*> o .:  "width"
        <*> o .:  "height"
        <*> o .:? "thumb"
        <*> o .:? "file_size"

data Video = Video
    { videoFileId   :: Text
    , videoWidth    :: Integer
    , videoHeight   :: Integer
    , videoDuration :: Integer
    , videoThumb    :: Maybe PhotoSize
    , videoMimeType :: Maybe Text
    , videoFileSize :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Video where
    parseJSON = withObject "Video" $ \o -> Video
        <$> o .:  "file_id"
        <*> o .:  "width"
        <*> o .:  "height"
        <*> o .:  "duration"
        <*> o .:? "thumb"
        <*> o .:? "mime_type"
        <*> o .:? "file_size"

data Voice = Voice
    { voiceFileId   :: Text
    , voiceDuration :: Integer
    , voiceMimeType :: Maybe Text
    , voiceFileSize :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Voice where
    parseJSON = withObject "Voice" $ \o -> Voice
        <$> o .:  "file_id"
        <*> o .:  "duration"
        <*> o .:? "mime_type"
        <*> o .:? "file_size"

data Contact = Contact
    { contactPhoneNumber :: Text
    , contactFirstName   :: Text
    , contactLastName    :: Maybe Text
    , contactUserId      :: Maybe Integer
    } deriving (Eq, Show)

instance FromJSON Contact where
    parseJSON = withObject "Contact" $ \o -> Contact
        <$> o .:  "phone_number"
        <*> o .:  "first_name"
        <*> o .:? "last_name"
        <*> o .:? "user_id"

data Location = Location
    { locationLongitude :: Double
    , locationLatitude  :: Double
    } deriving (Eq, Show)

instance FromJSON Location where
    parseJSON = withObject "Location" $ \o -> Location
        <$> o .: "longitude"
        <*> o .: "latitude"

data UserProfilePhotos = UserProfilePhotos
    { uppTotalCount :: Integer
    , uppPhotos     :: [[PhotoSize]]
    } deriving (Eq, Show)

instance FromJSON UserProfilePhotos where
    parseJSON = withObject "UserProfilePhotos" $ \o -> UserProfilePhotos
        <$> o .: "total_count"
        <*> o .: "photos"

data File = File
    { fileId   :: Text
    , fileSize :: Maybe Integer
    , filePath :: Maybe Text
    } deriving (Eq, Show)

instance FromJSON File where
    parseJSON = withObject "File" $ \o -> File
        <$> o .:  "file_id"
        <*> o .:? "file_size"
        <*> o .:? "file_path"

data InlineQuery = InlineQuery
    { inlineId     :: Text
    , inlineFrom   :: User
    , inlineQuery  :: Text
    , inlineOffset :: Text
    } deriving (Eq, Show)

instance FromJSON InlineQuery where
    parseJSON = withObject "InlineQuery" $ \o -> InlineQuery
        <$> o .: "id"
        <*> o .: "from"
        <*> o .: "query"
        <*> o .: "offset"

data ChosenInlineResult = ChosenInlineResult
    { cirId    :: Text
    , cirFrom  :: User
    , cirQuery :: Text
    } deriving (Eq, Show)

instance FromJSON ChosenInlineResult where
    parseJSON = withObject "ChosenInlineResult" $ \o -> ChosenInlineResult
        <$> o .: "result_id"
        <*> o .: "from"
        <*> o .: "query"
