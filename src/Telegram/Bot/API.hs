{-# LANGUAGE
    DataKinds
  , TypeFamilies
  , ScopedTypeVariables
  , FlexibleContexts
  #-}

module Telegram.Bot.API where

import           Data.Aeson (ToJSON(..), FromJSON(..), withObject, (.:), (.:?))
import qualified Data.Aeson as JSON (encode, eitherDecode)
import qualified Data.ByteString.Lazy.Char8 as BSL
import           Data.Maybe (fromMaybe)
import           Data.Monoid ((<>))
import           Data.Proxy (Proxy(..))
import           GHC.TypeLits (Symbol, KnownSymbol, symbolVal)
import qualified Network.HTTP.Conduit as HTTP
import qualified Network.HTTP.Types.Header as HTTP

data BotEnv = BotEnv
    { httpManager :: HTTP.Manager
    , apiToken :: String
    }

setup :: String -> IO BotEnv
setup token = BotEnv
    <$> HTTP.newManager HTTP.tlsManagerSettings
    <*> pure token

data (Show a, FromJSON a) => BotReply a = BotReply
    { replyOk :: Bool
    , replyDescription :: Maybe String
    , replyResult :: Maybe a
    } deriving (Show)

instance (Show a, FromJSON a) => FromJSON (BotReply a) where
    parseJSON = withObject "BotReply" $ \o -> BotReply
        <$> o .:  "ok"
        <*> o .:? "description"
        <*> o .:? "result"

class (ToJSON req, FromJSON (BotResponse req), Show (BotResponse req)) => BotRequest req where
    type BotMethod req :: Symbol
    type BotResponse req :: *

    invoke :: (KnownSymbol (BotMethod req))
           => BotEnv
           -> req
           -> IO (BotResponse req)
    invoke env req = do
        let method = symbolVal (Proxy :: Proxy (BotMethod req))
        let body = JSON.encode req
        print (method, body)

        reply <- query env method body

        print reply
        let br = either error id $ JSON.eitherDecode reply
        case br of
            BotReply True _ (Just r)  -> pure r
            BotReply False (Just e) _ -> error e
            _                         -> error $
                "Bad BotReply: " <> show br

query :: BotEnv -> String -> BSL.ByteString -> IO BSL.ByteString
query env method body = do
    request <- HTTP.parseUrl $ requestUrl (apiToken env) method
    let postRequest = request
            { HTTP.method = "POST"
            , HTTP.requestHeaders =
                [ (HTTP.hContentType, "text/json")
                , (HTTP.hAccept, "text/json")
                ]
            , HTTP.requestBody = HTTP.RequestBodyLBS body
            }
    reply <- HTTP.httpLbs request (httpManager env)
    pure $ HTTP.responseBody reply

requestUrl :: String -> String -> String
requestUrl token method = mconcat
    [ baseUrl
    , token
    , "/"
    , method
    ]
  where
     baseUrl = "https://api.telegram.org/bot"
