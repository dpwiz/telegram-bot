module Main where

import Control.Monad (forM_)
import System.Environment (getEnv)

import qualified Telegram.Bot.API as API
import           Telegram.Bot.Methods

main :: IO ()
main = do
    env <- API.setup =<< getEnv "TELEGRAM_TOKEN"

    API.invoke env GetMe >>= print

    updates <- API.invoke env $ GetUpdates Nothing Nothing Nothing
    forM_ updates $ \update -> do
        print update
        putStrLn ""
